class PasswordResetsController < ApplicationController
	before_action :get_user, only: [:edit, :update]
	before_action :valid_user, only: [:edit, :update]
	before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
  	@user = User.find_by(username: params[:password_reset][:username].downcase)
  	if @user
  		@user.create_reset_digest
  		@user.send_password_reset_email
      flash[:info] = "Check your email"
  		redirect_to root_url
  	else
      flash[:danger] = "The username provided does not exist"
  		render 'new'
  	end
  end

  def edit
  end

  def update
  	if password_blank?
      flash[:danger] = "Please provide a valid password"
  		render 'edit'
  	elsif @user.update_attributes(user_params)
  		log_in @user
      flash[:success] = "Password successfuly updated"
  		redirect_to @user
  	else
      flash[:danger] = "Issues"
  		render 'edit'
  	end
  end

  private
  	def user_params
  		params.require(:user).permit(:password, :password_confirmation)
  	end

  	def password_blank?
  		params[:user][:password].blank?
  	end

  	def get_user
  		@user = User.find_by(username: params[:username].downcase)
  	end

  	def valid_user
  		unless (@user && @user.activated? && @user.authenticated?(:reset, params[:id]))
        flash[:danger] = "Sorry, but you're not allowed"
  			redirect_to root_url
  		end
  	end

  	def check_expiration
  		if @user.password_reset_expired?
        flash[:danger] = "The link has expired"
  			redirect_to new_password_reset_url
  		end
  	end

end
