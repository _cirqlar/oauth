class AccountActivationsController < ApplicationController

	def edit
		user = User.find_by(username: params[:username])
		if user && !user.activated? && user.authenticated?(:activation, params[:id])
			user.activate
			log_in(user)
			flash[:success] = "Your account has successfully been activated"
			redirect_to user
		else
			flash[:danger] = "The link is invalid"
			redirect_to root_url
		end
	end

end
