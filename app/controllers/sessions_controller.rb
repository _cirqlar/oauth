class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by(username: params[:session][:username].downcase)
  	if user && user.authenticate(params[:session][:password])
  		if user.activated?
        log_in user
        params[:session][:remember_me]  ==  '1' ? remember(user)  : forget(user)
        flash[:success] = "You have successfully logged in :)" 
        redirect_back_or user
      else
        flash[:danger] = "Please activate your account"
        redirect_to root_url
      end
  	else
      flash[:danger] = "The username and/or password provided is/are incorrect"
  		render 'new'
  	end
  end

  def destroy
  	log_out if logged_in?
    flash[:success] = "You have successfully logged out."
  	redirect_to root_url
  end
end